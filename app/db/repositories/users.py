from typing import Optional

from app.db.errors import EntityDoesNotExist
from app.db.queries.queries import queries
from app.db.repositories.base import BaseRepository
from app.models.users import User, UserInDB


class UsersRepository(BaseRepository):
    async def get_user_by_email(self, *, email: str) -> UserInDB:
        user_row = await queries.get_user_by_email(self.connection, email=email)
        if user_row:
            return UserInDB(**user_row)

        raise EntityDoesNotExist(
            "user with email {0} does not exist".format(email))

    async def create_user(
        self,
        *,
        email: str,
        password: str,
    ) -> UserInDB:
        user = UserInDB(email=email)
        user.change_password(password)

        async with self.connection.transaction():
            user_row = await queries.create_new_user(
                self.connection,
                email=user.email,
                salt=user.salt,
                passhash=user.passhash,
            )

        return user.copy(update=dict(user_row))
