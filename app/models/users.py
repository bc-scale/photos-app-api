from typing import Optional

from app.models.mixins import DateTimeModelMixin
from app.utils import security
from pydantic import BaseModel, EmailStr, HttpUrl


class User(BaseModel):
    email: EmailStr


class UserInRequest(User):
    password: str


class UserWithToken(User):
    token: str


class UserInResponse(BaseModel):
    user: UserWithToken


class UserInDB(User, DateTimeModelMixin):
    id: int = 0
    salt: str = ""
    passhash: str = ""

    def check_password(self, password: str) -> bool:
        return security.verify_password(self.salt + password, self.passhash)

    def change_password(self, password: str) -> None:
        self.salt = security.generate_salt()
        self.passhash = security.get_password_hash(self.salt + password)
