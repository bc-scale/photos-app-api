class MediaDoesNotExist(Exception):
    """Raised when media file was not found in media storage."""
