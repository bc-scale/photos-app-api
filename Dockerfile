FROM python:3.9 as base
WORKDIR /code
COPY ./requirements.txt /code/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

# DEBUGGER
FROM base as development
RUN pip install debugpy
WORKDIR /code
CMD python -m debugpy --listen 0.0.0.0:5678 -m uvicorn app.main:app --host 0.0.0.0 --port 80 --reload

# PRODUCTION
FROM base as production
COPY ./app /code/app
CMD uvicorn app.main:app --host 0.0.0.0 --port 80